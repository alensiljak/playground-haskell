#!/usr/bin/env stack
-- stack --resolver lts-13.7 script

-- https://haskell.fpcomplete.com/get-started

main :: IO ()
main = putStrLn "Hello World"
